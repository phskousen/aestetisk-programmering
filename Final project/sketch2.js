let leftX, leftY, rightX, rightY;
let textL, textR;
let chosenPath;
let pathIndex = 0;

function preload() {
  data = loadJSON("JSON.json");
}

function setup() {
  createCanvas(windowWidth, windowHeight);
  //var definition
  initState  = data.initialStatement;
  leftPath   = data.leftPath;
  rightPath  = data.rightPath;
  //headline creation
  headline = new Headline(width/2, 200);
  subline  = new Headline(width/2, 500);
  t1 = initState.statement.toUpperCase()
  t2 = 'You went from'
  //button creation
  btnLeft  = new Button('A', width*0.33, height/2, 300, 150, 20);
  btnRight = new Button('B', width*0.66, height/2, 300, 150, 20);
  textL    = initState.A;
  textR    = initState.B;
}

function draw() {
  background(0);
  headline.show(t1);
  showBtn();
  endCon();
}

function showBtn() {
  btnLeft.show();
  btnRight.show();
  btnLeft.say(textL, 0, 0);
  btnRight.say(textR, 0, 0);
}

function endCon() {
  if((pathIndex >= leftPath[0].length || pathIndex >= leftPath[1].length)
  || pathIndex >= rightPath[0].length || pathIndex >= rightPath[1].length){
    clear();
    background(0);
    t1 = 'How did i end up here?';
    headline.show(t1);

    if (chosenPath === 'left') {
      if(textL = leftPath[0][length] && btnLeft.isClicked === true){
        subline.show(t2 + " " + "'" +initState.A + "'" + " to \n'Animals getting electrocuted'" );
      } else if (textR = leftPath[1][length] && btnRight.isClicked === true){
        subline.show(t2 + " " + "'" +initState.A + "'" + " to \n'Intruder killed after \ncharging occupant with a knife'" );
      }
    }
    if (chosenPath === 'right') {
      if(textL = rightPath[0][length] && btnLeft.isClicked === true){
        subline.show(t2 + " " + "'" +initState.B + "'" + " to \n'Police body cam \nof deadly shooting'" );
      } else if (textR = rightPath[1][length] && btnRight.isClicked === true){
        subline.show(t2 + " " + "'" +initState.B + "'" + " to \n'Why white lives matter more'" );
      }
    }
  }
}

function mousePressed() {
  btnLeft.clicked();
  btnRight.clicked();
  //Check initial decision; did the user choose the left or right path?
  if ((textL === initState.A || textR === initState.B)
   && btnLeft.isClicked === true) {
    print('left path chosen');
    chosenPath = 'left';
    pathIndex++;
    textL = leftPath[0][pathIndex];
    textR = leftPath[1][pathIndex];
  }
  else if ((textL === initState.A || textR === initState.B)
   && btnRight.isClicked === true){
    print('right path chosen');
    chosenPath = 'right';
    pathIndex++;
    textL = rightPath[0][pathIndex];
    textR = rightPath[1][pathIndex];
  }
  //Listen for button click; move to next item in corresponding array,
  //either from the left or right path, based on the inital decision
  if((btnLeft.isClicked === true || btnRight.isClicked === true)
   && chosenPath === 'left') {
    pathIndex++;
    textL = leftPath[0][pathIndex];
    textR = leftPath[1][pathIndex];
  }
  else if((btnLeft.isClicked === true || btnRight.isClicked === true)
   && chosenPath === 'right') {
    pathIndex++;
    textL = rightPath[0][pathIndex];
    textR = rightPath[1][pathIndex];
  }
}
