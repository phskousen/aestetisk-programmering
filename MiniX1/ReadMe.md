## MiniX1 assignment 1
### ***SPINNING GLOBE*** 🌍

![](https://gitlab.com/phskousen/aestheticprogramming/-/raw/6fce200c40b62f2d94077d06c42b9ef04733cc22/MiniX1/media/MiniX1_globe.gif)


- _What have you produced?_
    - For this first MiniX assigment as part of the course _Aesthetic programming_ at Aarhus University i have created an animated, rotating, three-dimensional wireframe sphere with a spinning ring orbiting it. Surrounding those objects is a dashed frame along with the words <br>"**SPINNING GLOBE**"

- _How would you describe your first independent coding experience (in relation to thinking, reading, copying, modifying, writing code, and so on)?_
    - Having some basic coding experience with HTML and CSS prior to this assignment, working with the MiniX has served mainly as an introduction to the programming language of p5.js. While the act of coding is not entirely new to me, this rather accessible language is. For me, what this means is that i already possess the privilege of being more or less confident working in an IDE as well as reading and writing code. This allowed me to spend more time learning the basic syntax of p5.js and less time getting to know the programming environment.

- _How is the coding process different from, or similar to, reading and writing text?_<br>
    - Growing up in a highly developed wealthy country, learning to both read and write in ones mother tongue at a very early age is a seemingly natural part of existence. Literacy is an intrinsic part of my ways of communication, and as a result, not being able to read or write seems almost unimaginable to me. This is not, however, a given for all people. Some don't have the privilege of education at an early age - let alone free education as is the case in Scandinavia and Germany along with some other countries within and outside of Europe.
As programming is not as of yet a mandatory or even widely recognized skill to be taught in primary school, learning to program - that is, learning to read and write an entirely new language and grammar - exposes some of the implications of illiteracy. What stands out to me in regards to programming is the fact that not being able to intuitively transfer my ideas and visions to another medium also means that i can't express myself as creatively and vividly as i would like to. The manifestation and execution of an idea that I have of a specific program is hindered by my lack of familiarity with the given programming language; for every line of code, I have to spend a great amount of time contemplating and researching how to perfom one specific task as well as understanding how that line of code logically works. This differs widely from the fluency with which I can write down my thoughts on a piece of paper or read the ideas of my peers.<br>
![](https://media4.giphy.com/media/oFPiPgqwof4Pe/giphy.gif?cid=ecf05e47hadb0bsbjuwtinmyb4qhlny1qupmxc7xpnwvi89r&rid=giphy.gif&ct=g)


- What does code and programming mean to you, and how does the assigned reading help you to further reflect on these terms?_
    - I like to think of myself as a fairly artistically creative person. I've always been able to best express myself through drawing, writing, composing music, filmmaking, photography and so on and so forth. In this regard, programming is no different; I see it as a means of expression, a tool which allows me to create and manifest ideas. The process of creating artifacts via code is not much different from the process of, say, composing music. The common denomitnator, it seems, is that those processes are iterative - one latches on to an idea, tries to give it a certain aesthetic, contemplates and evaulates, erases, changes and manipulates parts of that idea until one is either content with the results or goes on to work on another, more interesting idea.


For a review of the code, refer to this [gitlab repository](https://gitlab.com/phskousen/aestheticprogramming.git)

In order execute the program directly in your web browser, refer to this [gitlab.io link](https://phskousen.gitlab.io/aestheticprogramming/MiniX1) or the [p5.js web editor](https://editor.p5js.org/phskousen/full/XpQIQ_xml)
