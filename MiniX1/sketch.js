
function setup() {
  let canvas = createCanvas(500, 500, WEBGL);
//Refers to HTML div class = "canvas"
canvas.parent('canvasBox');
}

function draw() {
//Draws sphere and lets it rotate
  background(0);
  rotateY(millis()/2000);
  stroke(24, 242, 199);
  sphere(150);

//Draws torus and lets it rotate
  rotateX(-1.3);
  rotateZ(millis()/-1000);
  fill(0, 0, 0, 100);
  stroke(24, 242, 199);
  torus(200,8,24,6);
}
