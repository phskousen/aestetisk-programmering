# Aesthetic programming // Peter Skousen
This repository is the home for all assignments and experiments related to the aesthetic programming course in the Digital Design BA programme at Aarhus University.

![](https://media1.giphy.com/media/JIX9t2j0ZTN9S/giphy.gif?cid=ecf05e47oo2gcmlx05lu7p67cxt21l1yo2f7ed8i94y636d5&rid=giphy.gif&ct=g)
