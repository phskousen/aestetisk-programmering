
function setup(){
  createCanvas(windowWidth, windowHeight);
  noFill();
}

function draw() {
  background(0);
  strokeWeight(2);

  //Repeat the pattern by increments of 5px
  for(let h = 0; h<height; h+=5) {
    beginShape()
    //Draw points on the x axis from x=0 to x=width+130. Increment by 60px
    for (x = 0; x < width+130; x+=60) {
      stroke(random(255))
      //Use Perlin noise to define the coordinates of each y using x.
      //Scale it up 500 times.
      n = noise(x)*500;
      //Draw a line between the vertices defined in the for loop
      //and by the Perlin noise
      curveVertex(x-50,n+(h+100));
    }
    endShape();
  }
  noLoop();
}
