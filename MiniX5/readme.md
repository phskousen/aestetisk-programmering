## MiniX 5 - Generative program
![](https://gitlab.com/phskousen/aestheticprogramming/-/raw/main/MiniX5/media/Sk%C3%A6rmbillede%202022-03-20%20222548.png)

For an executable version of the program, please click [here](https://phskousen.gitlab.io/aestheticprogramming/MiniX5/)

To view the source code, please refer to the [Gitlab repository](https://gitlab.com/phskousen/aestheticprogramming/-/blob/main/MiniX5/sketch.js)
#### What are the rules in your generative program? Describe how your program performs over time? How do the rules produce emergent behavior?
For this MiniX 5, a program was written which procuces a randomly generated line-art image every time the program is executed. The lines on the screen are drawn randomly using Perlin noise to define random y coordinates for every x value. This line is drawn repeatly in increments of 5 pixels, and each line is given a random color value between 0 and 255. Summarised as a set rule of rules, it can be said that the program should: 
1. Draw a random curved line - full width - and assign it a random color value
2. Repeat the line downwards by increments of 5 pixels until it hits the bottom of the canvas, and assign it a new random color value

Although the program doesn't explicitly produce emergent behavior over a greater stretch of time, it does generate multiple lines within the for loops. I would have preferred to have the lines animate more vividly the longer the program runs, e.g. by having the lines interact with each other in some sort of way, or perhaps by animating the peaks of the waves up and down, with each movement of on line affecting the movement of the next one. This, unfortunately, I had a difficult time realising.

#### What role do rules and processes have in your work?
By having the program draw the y coordinates of the curves using Perlin noise, I am able to randomly generate a new image every time the program is executed. This, along with the fact that the colors assigned to each line is determined by the computer, makes for a program that is able to create an nearly infinite amount of seemingly unique images. This contrasts that of the human ability to choose random numbers or draw random lines on a canvas, as we are all prone to some sort of subjective predisposition in regard to what we _feel_ is random, as well as what we think _looks_ random.

#### Draw upon the assigned reading, how does this MiniX help you to understand the idea of “auto-generator” (e.g. levels of control, autonomy, love and care via rules)? Do you have any further thoughts on the theme of this chapter?
Using the `noise()` function within p5.js allows for the production of an _n_ amount of Perlin noise variations in the y positions of the curves' vertices. In this sense, it becomes a way of representing the formal indeterminacy ([Monfort et al., pp. 123](https://10print.org/10_PRINT_121114.pdf)) through a visual medium. But while it is true that a programmed piece of art on a computer can be generated randomly through functions affected by noise and RNGs, it is worth noting that the programmable art is excactly that - programmable. This means that while seemingly random (or really pseudo-random), the programmer, or artist, still possesses a large degree of control and influecne over the final image.
