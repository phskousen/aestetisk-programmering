## MiniX 7: Revisit the past

### Abstraction of an aurora

![](https://gitlab.com/phskousen/aestheticprogramming/-/raw/main/MiniX7/media/Sk%C3%A6rmbillede%202022-04-03%20kl.%2017.17.44.png)
![](https://gitlab.com/phskousen/aestheticprogramming/-/raw/main/MiniX7/media/Sk%C3%A6rmbillede_2022-04-03_kl._17.30.31.png)

#### Which MiniX have you reworked? What have you changed and why?
For this final individual MiniX, I have reworked my previous MiniX 6 on generative art. As with my original generative program, I've built upon the concept of noise, more particularly the `noise()` function built into p5.js, which is based on the Perlin noise algorithm. One of the challenges I face when working on my first generative program was that I had a hard time incorporating a temporal dimension into my program; that is, the program drew everything onto the canvas at once, leaving a randomly generated, yet static image printed onto the screen. For this revision, the program still uses a noise algorithm to (pseudo)randomly generate the values for each individual y-coordinate. This time, however, I have incorporated more movement into the program. Rather than having horisontal lines stack on top of each other, this program randomly generates an _n_ amount of vertical lines next to each other, each with a y-coordinate based on the noise algorithm. Each line is assigned a random length, which, when animated, allows for variation in the position of the lines in relation to each other. 

One major difference in my approach to this revision is the implementation of object-oriented programming. Wheras the original generative program utilized `for()` loops and the calling of various independent functions, with this program I used classes and objects to generate the images. This is noteworthy, as I found myself working quite differently in order to reach my goal. For this program, I was initially inspired by the natural phenomenon of northern lights, or auroras. When contemplating how I would tackle the challenge of writing a program which could simulate the visual qualities of the aurora, while still maintaining a degree of randomness, I found myself "breaking" the aurora into smaller pieces as it occured to me that an aurora could essentially be recreated by drawing a single horisontal, curved line onto which an array of perpendicular vertical lines could be placed. This, I thought, would give the illusion of an aurora, thus the reasoning for my object-oriented approach. As can be seen in the [sketch.js](https://gitlab.com/phskousen/aestheticprogramming/-/blob/main/MiniX7/sketch.js) and [classes.js](https://gitlab.com/phskousen/aestheticprogramming/-/blob/main/MiniX7/classes.js) files, this way of thinking is reflected in the code through the object instance `auroraLines` and class `Aurora()`:

    //classes.js
    class Aurora {
        [...]
    }

    //sketch.js
    function setup {
        [...]
        auroraLine[i] = new Aurora(initX+i*5, n, rl, 1, rcA);
    }
    

#### Aesthetic programming and digital culture
The program and its underlying code is one example of the particular programming approach that is aesthetic programming. As "a dynamic cultural practice and phenomenon, a way of thinking and doing in the world, and a means of understanding some of the complex procedures that underwrite and constitute our lived realities" (Soon & Cox, 2020, pp. 14), aesthetic programming encompasses the act of critically reflecting upon, perceiving and processing the world and the large variety of political, social, economic, cultural issues that are intrinsic to our (digital) existence. A stark exapmple of this can be observed in the case of object-oriented programming (OOP) through which the physcial world is abstracted through code. To draw a line between this notion and the described program, the physical phenomenon of the aurora is abstracted as digital objects which belong to a class. Thus, the aurora is transferred through digital media, consequently becoming something which can be broken into smaller pieces, manipulated, bent and stretched into new shapes, ideas, and aesthetic experiences.

To run the program, please [click here](https://phskousen.gitlab.io/aestheticprogramming/MiniX7/). <br>
For an overview of the source code, please click [here](https://gitlab.com/phskousen/aestheticprogramming/-/blob/main/MiniX7/sketch.js) for the sketch.js, and [here](https://gitlab.com/phskousen/aestheticprogramming/-/blob/main/MiniX7/classes.js) for the classes.js files respectively.

#### References
Soon, W, & Cox, G. (2020). _Aesthetic Programming: A Handbook of Software Studies_. Open Humanities Press 2020.
