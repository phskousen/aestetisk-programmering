class Aurora {
  constructor (_x, _y, _l, weight, _c) {
    this.x = _x;
    this.y = _y;
    this.length = _l;
    this.weight = weight;
    this.col = _c;
    this.speed = 2;
  }

  show () {
    strokeWeight (this.weight);
    stroke (this.col);
    beginShape (LINES);
    vertex (this.x, this.y);
    vertex (this.x, this.y + this.length);
    endShape ();
  }

  move () {
    this.y += this.speed;

    if (this.y+this.length >= height || this.y <= 0 ) {
      this.speed = -this.speed;
    }
  }
}
