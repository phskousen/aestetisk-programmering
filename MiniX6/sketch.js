let obstaclesL;
let obstaclesR;
let character;
let counter;
let rw = 600;

function setup() {
  createCanvas(windowWidth, windowHeight);
  character = new Character(width/2, height*0.90, 50);
  obstaclesL = new Obstacle(0, 0, rw, 50);
  obstaclesR = new Obstacle(rw+100, 0, width, 50);
  counter = new Counter(width/2, 100, 32);
  frameRate(60);
}

function draw() {
  background(0, 50, 100, 200);
  playerControl();
  collision();
  if (obstaclesL.y > height && obstaclesR.y > height) {
    rw = random(300, 700);
    obstaclesL.w = rw;
    obstaclesR.x = rw+100;
    obstaclesL.y = 0;
    obstaclesR.y = 0;
    obstaclesL.speed += 0.5;
    obstaclesR.speed += 0.5;
    character.speedMod += 0.2;
    counter.points++;
  }
  character.show();
  obstaclesL.show();
  obstaclesL.move();
  obstaclesR.show();
  obstaclesR.move();
  counter.show();
  counter.counterText = 'SCORE: '+counter.points;
}

function collision() {
  let charX1 = character.x-character.r/2;
  let charX2 = character.x+character.r/2;
  let charY  = character.y-character.r/2
  let obsY   = obstaclesL.y+50
  if (charX1 < obstaclesL.x+rw && charY < obsY ||
      charX2 > obstaclesR.x && charY < obsY) {
    background(255,30,80);
    obstaclesL.speed = 0;
    obstaclesR.speed = 0;
    counter.counterText = "GAME OVER";
  }
}

function playerControl() {
  if (keyIsDown(RIGHT_ARROW)) {
    character.x += 4 + character.speedMod;
  }
  if (keyIsDown(LEFT_ARROW)) {
    character.x -= 4 + character.speedMod;
  }
}
