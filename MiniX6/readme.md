## MiniX 6 – Games with objects

<img src="https://gitlab.com/phskousen/aestheticprogramming/-/raw/main/media/Sk%C3%A6rmbillede_2022-04-01_kl._15.49.28.png" width="50%" height="50%" align="left">
<img src="https://gitlab.com/phskousen/aestheticprogramming/-/raw/main/media/Sk%C3%A6rmbillede_2022-04-01_kl._15.48.30.png" width="50%" height="50%">

---

To execute the program, please click [here](https://phskousen.gitlab.io/aestheticprogramming/MiniX6/) <br>
To view the source code of the program, please refer to the [repository](https://gitlab.com/phskousen/aestheticprogramming/-/tree/main/MiniX6) for the [sketch.js](https://gitlab.com/phskousen/aestheticprogramming/-/blob/main/MiniX6/sketch.js) and [classes.js](https://gitlab.com/phskousen/aestheticprogramming/-/blob/main/MiniX6/classes.js) files respectively. 

#### The technical side of games
For this week's MiniX, I've created a small interactive game using javascript classes and objects -- so-called object-oriented programming (OOP). The program consists of four objects: a ball which the player controls using their arrow keys, a counter which keeps track of the current points collected, and two obstacles which both move along the y-axis at the same speed, and are spaced 70 pixels apart so that a small gap is formed between the two them. The player has to navigate the ball through this gap while the speed of the obstacles keeps increasing each round. The game ends if the player touches the obstacles with the ball, and a red game over screen is immediately displayed along with the final score. The obstacle objects are both part of the same class `Obstacles`. Likewise, the moveable ball is part of its own class `Character`, and the counter is part of the class `Counter`. When working with OOP in javascript, a few steps are neccessary in order to correctly create objects. Firstly, a class is named using the `class {}` syntax as can be seen in my program e.g. at line 18 of classes.js : 

    class Obstacle {
        [...]
    }

Next, the properties of the object must be defined. In this case, that means assigning variables which control the 

    class Obstacle {
        constructor(_x, _y, _w, _h) { //initalize the objects
            this.speed = 3;
            this.x = _x;
            this.y = _y;
            this.w = _w;
            this.h = _h;
        }
        [...]
    }

Thirdly, the behaviors, or _methods_, of the objects are defined as seen in the code below, which describes the functions `this.move()` and `this.show()`. Essentially, these functions define how the obstacles should move (along the y axis by this.speed amount of pixels) and what the objects look like in terms of shape, size and color.

    class Obstacle {
        [...]
        move() {
            this.y += this.speed;
        }
        show() {
            push();
            noStroke();
            fill(0);
            rect(this.x, this.y, this.w, this.h);
            pop();
        }
    }

Lastly, the individual objects need to actually be created, which is simply done by defining a new variable as a new _object instance_ of a class:

    let character;
    [...]
    character = new Character(width/2, height*0.90, 50);

#### Abstraction of objects
In object-oriented programming, objects are represented digitally through abstraction of its different features. Like in the phsycial world from which objects are "transferred" or represented, object instances and classes in OOP have names, properties, and behaviors. The properties are typically derived from the conventional descriptions and imaginaries of their real-life counterparts. This is not, however, a nessecity to the practice of OOP. Essential to the nature of OOP is that objects are abstractions; they are merely representations of phenomena - so too are their properties, names, descriptions, behaviours, etc., allowing for the manipulation and metamophosis of objects.
