let yellow, red, blue, grey;
let wc, hc;
let starCanvas;

function setup() {
  createCanvas(windowWidth, windowHeight);
  /* the createGraphics() function allows me to create a graphics buffer, 
     which enables me to create a static background on top of which all other elements are drawn */
  starCanvas = createGraphics(windowWidth, windowHeight); 
  /* placing the stars() function in the setup enables me to run the function only once, 
  resulting in a static background which is randomly generated every time the program is run */
  stars();
  wc =  windowWidth / 2;
  hc = windowHeight / 2;
  yellow = color(196, 164, 49);
  red    = color(196, 79,  82);
  blue   = color(69, 196, 176);
  grey   = color(200);
  frameRate(60);
}

function draw() {
  //draws the stars() function as the background using the createGraphics funtion- see setup as well as further below
  image(starCanvas,0,0); 
  /* calls the custom orbit() and planets() functions as seen further below. 
  Instead of writing all syntax in draw(), this helps me structure the code */
  orbit();
  planets();
}

function orbit() {
  /* dashed line base javascript function from user squishynotions
  https://editor.p5js.org/squishynotions/sketches/Ax195WTdz */
  function setLineDash(list) {
    drawingContext.setLineDash(list);
  }
  //create dashed circles
  noFill();
  strokeWeight(1);
  stroke(255);

  setLineDash([7, 21]); //apply the dashed line property
  ellipse(wc, hc, 150); //create the ellipse itself 
  setLineDash([7, 27]);
  ellipse(wc, hc, 250);
  setLineDash([7, 37]);
  ellipse(wc, hc, 350);
}


function planets() {
  noStroke();
  fill(yellow);
  ellipse(wc, hc, 60);

  let num = 300;     //create variable "num", which divides the circle into num = 300 segments
  translate(wc, hc); //push the follow code to the center
  let cir = 360/num*(frameCount%num); // variable to enable the rotation of the ellipses in relation to the current framecount

  rotate(radians(cir)); //rotate the ellipses (planets) around the origin
  fill(grey);
  ellipse(0, -175, 25);

  rotate(radians(cir));
  fill(red);
  ellipse(0, -125, 10);

  rotate(radians(cir));
  fill(blue);
  ellipse(0, -75, 20);
}

function stars(){
  /* Here, an object oriented syntax i used in order to draw the background.
     In order to draw 500 stars with random x and y values, a for loop is used
     along with the random() function */
  starCanvas.background(1, 32, 48, 80); 
  for(let i = 0; i < 500; i++) {
    starCanvas.stroke(255,50);
    starCanvas.strokeWeight(random(4));
    let rx = random(width);
    let ry = random(height);
    starCanvas.point(rx,ry);
  }
}
