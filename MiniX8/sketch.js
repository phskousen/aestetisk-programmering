let falseNarratives;
let i = 0;
let a = 0;

function preload() {
  falseNarratives = loadJSON('falseNarratives.json');
}

function  setup() {
  falseNarrative = falseNarratives.QA;
  createCanvas(windowWidth, windowHeight);
  frameRate(60);
}

function draw() {
  background(0);
  fill(255);
  textSize(32);

  secondsTimer();
  createNarrative();
}

function secondsTimer() {
  if (frameCount % 180 == 0) {
    i++;
  }
  if (i >= falseNarrative.length){
    i = 0;
  }
}

function createNarrative() {
  text(falseNarrative[i].question, width/2-100, height/2);
  text(falseNarrative[i].falseAnswer, width/2-100, height/2+50);
}
