# falseNarratives

![](https://gitlab.com/phskousen/aestheticprogramming/-/raw/main/MiniX8/media/Sk%C3%A6rmbillede%202022-04-21%20kl.%2015.50.15.png)
<br>
![](https://gitlab.com/phskousen/aestheticprogramming/-/blob/main/MiniX8/media/Sk%C3%A6rmbillede%202022-04-21%20kl.%2015.50.23.png)

For this MiniX8 we wanted to do something to tell one story on the frontside while keeping the crucial details on the backside. This is a way of telling the users of our MiniX that they should be critical about the information that they choose to accept and believe as truthful.

As for the frontend a set of messages and answers will be displayed accordingly so that they match. This will show a typical real life scenario for two people speaking with each other. However the interesting thing from the conversations we have with other humans is the things we actually do not say. It is the things we choose to keep for ourselves because of various reasons such as the cultural norm.
“When one speaks the source code, it performs differently than how a machine performs. Yet it should be said that this is the case with humans too in that there is also an interface and translation between physiognomy and action. (Winnie Soon, 2021) in this code, Soon explains how the code acts differently when being spoken than when it’s run on the computer. Certain things don't show on the computer, and this makes room for poetry and hidden text/meaning.
If we however choose to go a layer deeper we see the real thoughts of a conversation. This can only be seen on the backside since our runme will not display these answers. This type of approach can be compared to a lot of modern media sources and is an issue to be aware of.


#### Describe how your program works, and what syntax you have used, and learnt?
To create the minix we have been introduced to the JSON file, and how it can be used to store (most often) text, to separate it from the rest of the code. The program in itself consists of questions and (false)answers, which are plotted randomly into the program. If you then dig in and decode it, you realize in the JSON file there is another answer, the (true)answer, which is not visible in the program.
For the syntaxes we have used an “if-else” syntax to call for the changing text-colors. This makes it seem like the text is ducking in and out. When the text from the json file changes it switches from black to white to black again which gives this smooth effect. We have also used syntaxes as preload to make the program run without interruptions and additionally also a framerate at 60 to make the screen update often and create a smooth experience.


#### Analyze your own e-lit work by using the text Vocable Code and/or The Aesthetics of Generative Code (or other texts that address code/voice/language).


Our project is a contrast to the statement mentioned in Winnie Soons book Aesthetic Programming in the chapter Vocable Code. Here they discuss WYSIWYG, which means what you see is what you get, a term referring to ordinary programming, meaning every line of code has an effect on the result, and shows up on the runme. What we have done though is the complete opposite, with the code displayed on the runme not equaling the code.
“In this way, the artwork perhaps challenges the usual, prominent front-end interfaces and the transmission of meaning from one source to another by giving voice to both the front and back ends, or even queering the demarcation. “ (Winnie Soon, 2021)
Now, with this type of code all the attention isn’t on the program anymore, but more so on the actual code. The code hides a bigger meaning, something hidden, so only the ones you dig in and actually read the code will understand it. We as a group discussed how the minix might be made upon small things like: (question)“How are you?” (answer)”I’m fine,” (real answer)”I feel bad,” yet the meaning of our work goes much beyond that. One might say that this is a “metaphor” for a whole lot of things happening around the world, examples being; the media, governments, and powerful people. Whether it is manipulation of information or pure withholding, don’t believe in everything you see or hear, something might just be hidden, just like the code.
“countering the automated response is not facilitated by the silence of an artwork but by its noise, by its confrontation of silence, perhaps even by its disordered syntax, rhythm, and spelling.” Rita Raley (2002) ekstra god kilde

Our code draws upon political aspects, such as manipulation and withholding of information in different degrees is a big part of everything going on. We tried to shed light on this by taking an everyday question and answer, and showing how not sharing full information can both be big and small, and can affect very few people, but also affect countries and unions, like the mink situation with Mette Frederiksen. She held back information for her own good, just as Putin censors the media to manipulate the country. We experience a lot of that right now, and wanted to express it in a lighter format with our code.


For a review of the code, see the repository: [JSON here](https://gitlab.com/phskousen/aestheticprogramming/-/blob/main/MiniX8/falseNarratives.json), and [sketch.js here](https://gitlab.com/phskousen/aestheticprogramming/-/blob/main/MiniX8/sketch.js)
<br>
For an executable version of the program, [click here](https://phskousen.gitlab.io/aestheticprogramming/MiniX8/)
