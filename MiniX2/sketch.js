function setup() {
  let cnv = createCanvas(windowWidth, windowHeight);
  cnv.parent('js');
  textAlign(CENTER,CENTER);
  textSize(width/15);
}

let eL_ran = Math.PI;
let eR_ran = Math.PI;
let cY;
let col = 127;

function draw() {
  // VAR DEF
  let posL   = windowWidth  * 0.33;
  let posR   = windowWidth  * 0.66;
  let mY     = windowHeight * 0.55;

  background(col);
  //EYES
  fill(255);
  noStroke();
  ellipseMode();
  arc(posL, height*0.4, 80, 80, 0, eL_ran, OPEN);
  arc(posR, height*0.4, 80, 80, 0, eR_ran, OPEN);
  //MOUTH
  noFill();
  stroke(255);
  strokeWeight(30);
  curve(0,cY,  width*0.25,mY,  width*0.75,mY,  width,cY);
  fill(0);
}

function mouseClicked(){
  eL_ran = random([PI,PI+QUARTER_PI,TWO_PI]);
  eR_ran = random([PI,PI+QUARTER_PI,TWO_PI]);
  cY    = random(-70,700);

  let g = color(93, 140, 123);
  let y = color(242, 208, 145);
  let o = color(242, 166, 121);
  let r = color(217, 105, 95);
  let dR= color(140, 70, 70);
  col   = random([g,y,o,r,dR]);
}
